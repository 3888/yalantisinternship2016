package com.intership.yalantis.com.taskone.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.intership.yalantis.com.taskone.adapters.MainListViewAdapter;
import com.intership.yalantis.com.taskone.models.AllocationData;
import com.intership.yalantis.com.taskone.R;
import com.intership.yalantis.com.taskone.utils.ScrollDetectingListView;

import butterknife.Bind;
import butterknife.ButterKnife;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListViewFragment extends Fragment {

    @Bind(R.id.task_list_view)
    ScrollDetectingListView mList;

    FabBehavior mCallback;

    private List<AllocationData> mData;

    public ListViewFragment() {
    }

    public interface FabBehavior {
        void setFabVisibility(boolean scroll);
    }


    @Override
    public void onAttach(Context Context) {
        super.onAttach(Context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (FabBehavior) Context;
        } catch (ClassCastException e) {
            throw new ClassCastException(Context.toString()
                    + " must implement FabBehavior");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Любая работа с данными должна быть вынесена из класса отвечающего за UI
        List<String> likesCount = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.likes_count)));
        List<String> headline = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.headline)));
        List<String> address = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.address)));
        List<String> date = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.date)));
        List<String> daysLeft = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.days_left)));

        mData = new ArrayList<>(likesCount.size());
        for (int i = 0; i < likesCount.size(); i++) {
            AllocationData element = new AllocationData(likesCount.get(i), headline.get(i),
                    address.get(i), date.get(i), daysLeft.get(i));
            mData.add(element);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_view, container, false);
        ButterKnife.bind(this, view);

        MainListViewAdapter adapter = new MainListViewAdapter(getContext(), mData);
        mList.setAdapter(adapter);
        mList.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mInitialScroll = 0;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //get actual scroll position
                int scrolledOffset = mList.getVerticalScrollOffset();
                if (scrolledOffset != mInitialScroll) {
                    boolean scrollUp = (scrolledOffset - mInitialScroll) < 0;
                    mInitialScroll = scrolledOffset;

                    mCallback.setFabVisibility(scrollUp);
                }
            }
        });

        return view;
    }
}