package com.intership.yalantis.com.taskone.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.intership.yalantis.com.taskone.adapters.TaskRecyclerViewAdapter;
import com.intership.yalantis.com.taskone.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskOneActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.task_toolbar)
    Toolbar mToolbar;
    @Bind(R.id.task_recycler_view)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_scrolling);

        initResources();
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (mToolbar != null) {
            mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.task_title));
        }
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, v.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }

    private void initResources() {
        ButterKnife.bind(this);

        initToolbar();
        List<String> imageUrlList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.image_url_list)));

        TaskRecyclerViewAdapter adapter = new TaskRecyclerViewAdapter(imageUrlList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        if (mRecyclerView != null) {
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setAdapter(adapter);
        }
    }

    @OnClick({R.id.task_toolbar, R.id.task_name_text_view, R.id.task_status, R.id.task_created_label_text_view,
            R.id.task_date_of_create_text_view, R.id.task_registered_label_text_view,
            R.id.task_date_of_registration_text_view, R.id.task_solve_till_label_text_view,
            R.id.task_date_of_solving_text_view, R.id.task_responsible_label_text_view,
            R.id.task_responsible_name_text_view, R.id.task_divider, R.id.task_details_text_view})

    public void buttonClicks(View view) {
        switch (view.getId()) {
            default:
                Toast.makeText(this, view.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
        }
    }
}