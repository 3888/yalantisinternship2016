package com.intership.yalantis.com.taskone.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intership.yalantis.com.taskone.adapters.MainRecyclerViewAdapter;
import com.intership.yalantis.com.taskone.models.AllocationData;
import com.intership.yalantis.com.taskone.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecyclerViewFragment extends Fragment {

    private List<AllocationData> mData;

    public RecyclerViewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Любая работа с данными должна быть вынесена из класса отвечающего за UI
        List<String> likesCount = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.likes_count)));
        List<String> headline = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.headline)));
        List<String> address = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.address)));
        List<String> date = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.date)));
        List<String> daysLeft = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.days_left)));

        mData = new ArrayList<>(likesCount.size());
        for (int i = 0; i < likesCount.size(); i++) {
            AllocationData element = new AllocationData(likesCount.get(i), headline.get(i),
                    address.get(i), date.get(i), daysLeft.get(i));
            mData.add(element);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.task_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        MainRecyclerViewAdapter adapter = new MainRecyclerViewAdapter(mData);
        recyclerView.setAdapter(adapter);

        return view;
    }
}