package com.intership.yalantis.com.taskone.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.intership.yalantis.com.taskone.adapters.SectionsViewPagerAdapter;
import com.intership.yalantis.com.taskone.fragments.ListViewFragment;
import com.intership.yalantis.com.taskone.fragments.RecyclerViewFragment;
import com.intership.yalantis.com.taskone.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ListViewFragment.FabBehavior {

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.nav_view)
    NavigationView mNavigationView;
    @Bind(R.id.main_toolbar)
    Toolbar mToolbar;
    @Bind(R.id.container)
    ViewPager mViewPager;
    @Bind(R.id.main_app_bar_tab)
    TabLayout mTabLayout;
    @Bind(R.id.nav_view_footer_authors_text_view)
    TextView mFooterLink;
    @Bind(R.id.fab)
    FloatingActionButton mFab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initResources();

        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView);
        }

        if (mViewPager != null) {
            setupViewPager(mViewPager);
        }
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.navigation_drawer_allocution));
        }
    }

    private void initResources() {
        ButterKnife.bind(this);
        initToolbar();
        mFooterLink.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsViewPagerAdapter adapter = new SectionsViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RecyclerViewFragment(), getResources().getString(R.string.main_tab_at_work));
        adapter.addFragment(new RecyclerViewFragment(), getResources().getString(R.string.main_tab_done));
        adapter.addFragment(new ListViewFragment(), getResources().getString(R.string.main_tab_waiting));
        viewPager.setAdapter(adapter);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setFabVisibility(boolean scroll) {

        if (scroll) {
            mFab.show();
        } else {
            mFab.hide();
        }
    }
}