package com.intership.yalantis.com.taskone.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.intership.yalantis.com.taskone.R;

import butterknife.Bind;
import butterknife.ButterKnife;

import java.util.List;

public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskRecyclerViewAdapter.ViewHolder> {

    private List<String> mImageUrlList;

    /**
     * Whereas an ArrayAdapter requires a context to be passed into it's constructor, a RecyclerView.Adapter does not.
     * Instead, the correct context can inferred from the parent view when inflation is necessary.
     */
    public TaskRecyclerViewAdapter(List<String> list) {
        mImageUrlList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_recycler_view_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mImageUrlList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        String url = mImageUrlList.get(position);

        final Context context = viewHolder.itemView.getContext();

        Glide.with(context)
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.drawable.ic_alert)
                .centerCrop()
                .into(viewHolder.mImageView);

        viewHolder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, v.getClass().getSimpleName(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.fragment_image_view_list_item)
        ImageView mImageView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}