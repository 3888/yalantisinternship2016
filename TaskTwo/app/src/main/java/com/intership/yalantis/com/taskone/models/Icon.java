package com.intership.yalantis.com.taskone.models;

import android.support.annotation.DrawableRes;

/**
 * The available avatars with their corresponding drawable resource ids.
 */
public enum Icon {

    ONE(android.R.drawable.ic_menu_edit),
    TWO(android.R.drawable.ic_menu_zoom),
    THREE(android.R.drawable.ic_menu_compass),
    FOUR(android.R.drawable.ic_menu_myplaces),
    FIVE(android.R.drawable.ic_menu_today),
    SIX(android.R.drawable.ic_menu_agenda),
    SEVEN(android.R.drawable.ic_menu_camera),
    EIGHT(android.R.drawable.ic_menu_manage),
    NINE(android.R.drawable.ic_menu_call),
    TEN(android.R.drawable.ic_menu_gallery);

    private final int mResId;

    Icon(@DrawableRes final int resId) {
        mResId = resId;
    }

    @DrawableRes
    public int getDrawableId() {
        return mResId;
    }
}
