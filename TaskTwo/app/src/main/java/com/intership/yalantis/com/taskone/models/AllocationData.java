package com.intership.yalantis.com.taskone.models;

public class AllocationData {

    private String mLikesCount;
    private String mHeadLine;
    private String mAddress;
    private String mDate;
    private String mDaysLeft;

    public AllocationData(String likesCount, String headLine, String address,
                          String date, String daysLeft) {
        mLikesCount = likesCount;
        mHeadLine = headLine;
        mAddress = address;
        mDate = date;
        mDaysLeft = daysLeft;
    }

    public String getLikesCount() {
        return mLikesCount;
    }

    public String getHeadLine() {
        return mHeadLine;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getDate() {
        return mDate;
    }

    public String getDaysLeft() {
        return mDaysLeft;
    }
}